# SteamBall

The sources of the SteamBall project made on Unreal Engine 4.23 with the "SteamVR input for unreal" plugin.  
*Les sources du projet SteamBall fait avec Unreal Engine 4.23 et l'extension SteamVR input for unreal.*

This game as been made for an educational project by Bruno FACHE, Benoît LAMARRE and Bastien PROB.  
*Ce jeu a été fait par Bruno FACHE, Benoît LAMARRE et Bastien PROB dans le cadre d'un projet éducatif.*

You can find the executable for windows 10 64bit [here](https://gitlab.com/steamball/steamball-exe).  
*Vous pouvez trouver l'éxécutable pour windows 10 64bit [ici](https://gitlab.com/steamball/steamball-exe).*

## What is it ?
This game is a 3D flipper game in virtual reality. You controls two planks and you have to hit the balls in order to maintain them in the level. 
Make them bounce on structure as much as yu can to earn points. There are special quests that will make you earn jackpots.
If you lose all the balls, the game is over. You have to try to beat your highscore.  

*SteamBall est un flipper 3D en realité virtuelle. Vous controlez deux palets et vous devez taper les balles pour les conserver dans la zone de jeu.*
*Faites-les rebondir sur les objects pour gagner des points. Des quêtes spéciales peuvent vous faire remporter des récompenses spéciales.*
*Si vous perdez toutes les balles, la partie est finie. Faites exploser le compteur de score !*


## Gameplay demo video / *Vidéo de demonstration*
A gameplay demonstration video available in french on [youtube](https://www.youtube.com/watch?v=ds3Cbeal-sQ).  
*Une vidéo de démonstration disponible sur [youtube](https://www.youtube.com/watch?v=ds3Cbeal-sQ).*  
[![youtube](https://img.youtube.com/vi/ds3Cbeal-sQ/0.jpg)](https://www.youtube.com/watch?v=ds3Cbeal-sQ).